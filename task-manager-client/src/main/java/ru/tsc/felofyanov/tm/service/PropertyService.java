package ru.tsc.felofyanov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.tsc.felofyanov.tm.api.service.IPropertyService;

import java.util.Properties;

@Getter
@Service
public class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private final Properties properties = new Properties();

    @NotNull
    @Value("#{environment['server.port'] ?: '8080'}")
    private String serverPort;

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }
}
