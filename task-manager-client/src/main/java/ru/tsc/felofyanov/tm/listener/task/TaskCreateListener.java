package ru.tsc.felofyanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.TaskCreateRequest;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Date;

@Component
public final class TaskCreateListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    @EventListener(condition = "@taskCreateListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[TASK CREATE]");

        System.out.println("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();

        System.out.println("ENTER DATE BEGIN: ");
        @Nullable final Date dateBegin = TerminalUtil.nextDate();

        System.out.println("ENTER DATE END: ");
        @Nullable final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final TaskCreateRequest request =
                new TaskCreateRequest(getToken(), name, description, dateBegin, dateEnd);
        getTaskEndpoint().createTask(request);
    }
}
