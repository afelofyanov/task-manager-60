package ru.tsc.felofyanov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractResultResponse {

    public UserUnlockResponse(@NotNull Throwable throwable) {
        super(throwable);
    }
}
