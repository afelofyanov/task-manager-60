package ru.tsc.felofyanov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.felofyanov.tm.api.service.dto.ITaskDTOService;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;
import ru.tsc.felofyanov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.UserIdEmptyException;

import java.util.List;
import java.util.Optional;

@Service
public class TaskDTOService extends AbstractUserOwnedDTOService<TaskDTO, ITaskDTORepository> implements ITaskDTOService {

    @NotNull
    @Autowired
    private ITaskDTORepository taskRepository;

    @NotNull
    @Override
    protected ITaskDTORepository getRepository() {
        return taskRepository;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(TaskIdEmptyException::new);
        @NotNull final ITaskDTORepository repository = getRepository();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        @NotNull final ITaskDTORepository repository = getRepository();
        repository.removeAllByProjectId(userId, projectId);
    }
}
