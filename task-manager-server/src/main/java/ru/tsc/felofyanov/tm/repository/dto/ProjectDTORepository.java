package ru.tsc.felofyanov.tm.repository.dto;

import lombok.Getter;
import org.springframework.stereotype.Repository;
import ru.tsc.felofyanov.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;

@Getter
@Repository
public class ProjectDTORepository extends AbstractUserOwnerDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository() {
        super(ProjectDTO.class);
    }
}
