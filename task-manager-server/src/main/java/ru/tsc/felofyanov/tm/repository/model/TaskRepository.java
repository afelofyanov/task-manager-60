package ru.tsc.felofyanov.tm.repository.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.felofyanov.tm.api.repository.model.ITaskRepository;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;

@Getter
@Repository
public class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    public TaskRepository() {
        super(Task.class);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager.createQuery("FROM " + getModelName() + " m WHERE m.user.id = :userId AND m.project.Id = :projectId", clazz)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public final void removeAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        entityManager.createQuery("DELETE FROM " + getModelName() + " m WHERE m.user.id = :userId AND m.project.id = :projectId")
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .executeUpdate();
    }
}
