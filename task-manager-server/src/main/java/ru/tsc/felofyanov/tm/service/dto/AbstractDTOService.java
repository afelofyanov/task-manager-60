package ru.tsc.felofyanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.api.repository.dto.IDTORepository;
import ru.tsc.felofyanov.tm.api.service.dto.IServiceDTO;
import ru.tsc.felofyanov.tm.dto.model.AbstractModelDTO;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.IdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.IndexIncorrectException;

import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>> implements IServiceDTO<M> {

    @NotNull
    protected abstract IDTORepository<M> getRepository();

    @Override
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.add(model);
    }

    @Override
    @Transactional
    public Collection<M> add(@Nullable Collection<M> models) {
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.add(models);
    }

    @Override
    @Transactional
    public M update(@Nullable M model) {
        if (model == null) return null;
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.update(model);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models.isEmpty()) return models;
        @NotNull final IDTORepository<M> repository = getRepository();
        repository.clear();
        return repository.add(models);
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final IDTORepository<M> repository = getRepository();
        repository.clear();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.findOneById(id);
    }

    @Override
    @Nullable
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.findOneByIndex(index);
    }

    @Override
    @Transactional
    public M remove(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.remove(model);
    }

    @Override
    @Transactional
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.removeById(id);
    }

    @Override
    @Transactional
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.removeByIndex(index);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final IDTORepository<M> repository = getRepository();
        repository.removeAll(collection);
    }

    @Override
    public long count() {
        @NotNull final IDTORepository<M> repository = getRepository();
        return repository.count();
    }
}
