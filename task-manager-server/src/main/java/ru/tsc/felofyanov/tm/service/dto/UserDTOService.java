package ru.tsc.felofyanov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.felofyanov.tm.api.service.IPropertyService;
import ru.tsc.felofyanov.tm.api.service.dto.IUserDTOService;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.util.HashUtil;

@Service
@AllArgsConstructor
public class UserDTOService extends AbstractDTOService<UserDTO, IUserDTORepository> implements IUserDTOService {

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @NotNull
    @Autowired
    private final IUserDTORepository userRepository;

    @NotNull
    @Override
    protected IUserDTORepository getRepository() {
        return userRepository;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final IUserDTORepository repository = getRepository();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.add(user);
        return user;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public UserDTO createWithEmail(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new LoginExistException();
        if (isEmailExists(email)) throw new EmailExistException();

        @NotNull final UserDTO user = create(login, password);
        user.setEmail(email);
        update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO createWithRole(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final UserDTO user = createWithEmail(login, password, email);
        user.setRole(role);
        update(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDTORepository repository = getRepository();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final IUserDTORepository repository = getRepository();
        return repository.findByEmail(email);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(userId);
        if (user == null) return null;
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        update(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (firstName == null || firstName.isEmpty()) throw new FIOEmptyException();
        if (middleName == null || middleName.isEmpty()) throw new FIOEmptyException();
        if (lastName == null || lastName.isEmpty()) throw new FIOEmptyException();

        @Nullable final UserDTO user = findOneById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDTORepository repository = getRepository();
        repository.removeByLogin(login);
    }
}
