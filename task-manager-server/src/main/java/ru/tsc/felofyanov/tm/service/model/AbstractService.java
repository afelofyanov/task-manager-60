package ru.tsc.felofyanov.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.api.repository.model.IRepository;
import ru.tsc.felofyanov.tm.api.service.model.IService;
import ru.tsc.felofyanov.tm.exception.entity.ModelEmptyException;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.IdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.IndexIncorrectException;
import ru.tsc.felofyanov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected abstract IRepository<M> getRepository();

    @Override
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final IRepository<M> repository = getRepository();
        return repository.add(model);
    }

    @Override
    @Transactional
    public Collection<M> add(@Nullable Collection<M> models) {
        @NotNull final IRepository<M> repository = getRepository();
        return repository.add(models);
    }

    @Override
    @Transactional
    public M update(@Nullable M model) {
        if (model == null) return null;
        @NotNull final IRepository<M> repository = getRepository();
        return repository.update(model);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final IRepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models.isEmpty()) return models;
        @NotNull final IRepository<M> repository = getRepository();
        repository.clear();
        return repository.add(models);
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final IRepository<M> repository = getRepository();
        repository.clear();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IRepository<M> repository = getRepository();
        return repository.findOneById(id);
    }

    @Override
    @Nullable
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final IRepository<M> repository = getRepository();
        return repository.findOneByIndex(index);
    }

    @Override
    @Transactional
    public M remove(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final IRepository<M> repository = getRepository();
        return repository.remove(model);
    }

    @Override
    @Transactional
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final IRepository<M> repository = getRepository();
        @Nullable final M result = repository.removeById(id);
        if (result == null) throw new ModelEmptyException();
        return result;
    }

    @Override
    @Transactional
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();

        @NotNull final IRepository<M> repository = getRepository();
        @Nullable final M result = repository.removeByIndex(index);
        if (result == null) throw new ModelEmptyException();
        return result;
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final IRepository<M> repository = getRepository();
        repository.removeAll(collection);
    }

    @Override
    public long count() {
        @NotNull final IRepository<M> repository = getRepository();
        return repository.count();
    }
}

