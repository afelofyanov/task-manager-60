package ru.tsc.felofyanov.tm.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.felofyanov.tm.api.service.IDatabaseProperty;
import ru.tsc.felofyanov.tm.dto.model.AbstractModelDTO;
import ru.tsc.felofyanov.tm.model.AbstractModel;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ComponentScan("ru.tsc.felofyanov.tm")
@PropertySource("classpath:application.properties")
@EnableTransactionManagement(proxyTargetClass = true)
public class ServerConfiguration {

    @Autowired
    private IDatabaseProperty databaseProperty;

    @Bean
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseProperty.getDatabaseDriver());
        dataSource.setUrl(databaseProperty.getDatabaseUrl());
        dataSource.setUsername(databaseProperty.getDatabaseUserName());
        dataSource.setPassword(databaseProperty.getDatabaseUserPassword());
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan(
                AbstractModel.class.getPackage().getName(),
                AbstractModelDTO.class.getPackage().getName()
        );
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DRIVER, databaseProperty.getDatabaseDriver());
        properties.put(Environment.URL, databaseProperty.getDatabaseUrl());
        properties.put(Environment.USER, databaseProperty.getDatabaseUserName());
        properties.put(Environment.PASS, databaseProperty.getDatabaseUserPassword());
        properties.put(Environment.DIALECT, databaseProperty.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHbm2ddlAuto());
        properties.put(Environment.SHOW_SQL, databaseProperty.getDatabaseShowSql());
        properties.put(Environment.FORMAT_SQL, databaseProperty.getDatabaseFormatSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseUseSecondL2Cache());
        properties.put(Environment.USE_QUERY_CACHE, databaseProperty.getDatabaseUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getDatabaseUseMinimalPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getDatabaseRegionPrefix());
        properties.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getDatabaseRegionFactoryClass());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseProviderConfigurationFileResourcePath());

        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }
}
